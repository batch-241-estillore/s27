let users = `{
	"userId": "1",
	"firstName": "Gab",
	"email": "gab@email.com",
	"password": "securepassword",
	"isAdmin": "true",
	"mobileNumber": "09123456789"
}`;
let order = `{
	"userId": "1",
	"transDate": "01/26/23",
	"status": "shipped",
	"total": "1000"
}`
let products = `{
	"name": "Toy car",
	"description": "looks like real car but small",
	"price": "1000",
	"stocks": "1",
	"isActive": "true",
	"sku": "1"
}`
let orderProducts = `{
	"orderID": "0001",
	"productID": "01",
	"quantity": "1",
	"price": "1000",
	"subTotal": "1200"
}`

